<#import "template.ftl" as layout>
<@layout.registrationLayout displayMessage=false; section>
    <#if section = "header">
        ${msg("termsTitle")}
    <#elseif section = "form">
    <div id="kc-terms-text">
    

    <section id="page-content-section" class="page-content-section"><section>
<h2>Legal information</h2>

<h2>Deutsches Forschungszentrum für Künstliche Intelligenz GmbH, <abbr title="German Research Center for Artificial Intelligence">DFKI </abbr><br>
(German Research Center for Artificial Intelligence)</h2>

<dl>
	<dt>Postal Address</dt>
	<dd>
	<address>Postfach 20 80<br>
	D-67608 Kaiserslautern</address>
	</dd>
	<dt>E-Mail</dt>
	<dd><a href="http://mailto:info@dfki.de" target="_blank">info@dfki.de</a></dd>
	<dt>Contact person for the Website</dt>
	<dd><a href="http://mailto:georg.rehm@dfki.de" target="_blank">georg.rehm@dfki.de</a></dd>
</dl>

<dl>
	<dt>Kaiserslautern Site</dt>
	<dd>
	<address>Trippstadter Straße 122<br>
	D-67663 Kaiserslautern</address>
	</dd>
	<dt>Phone</dt>
	<dd>+49 (0)631 / 205 75-0</dd>
	<dt>Telefax</dt>
	<dd>+49 (0)631 / 205 75-5030</dd>
</dl>

<dl>
	<dt>Saarbruecken Site</dt>
	<dd>
	<address>Campus D3_2<br>
	Stuhlsatzenhausweg 3 (Building D3 2)<br>
	D-66123 Saarbruecken</address>
	</dd>
	<dt>Phone</dt>
	<dd>+49 (0)681 / 857 75-0</dd>
	<dt>Telefax</dt>
	<dd>+49 (0)681 / 857 75-5341</dd>
</dl>

<dl>
	<dt>Bremen Site</dt>
	<dd>
	<address>Robert-Hooke-Straße 1<br>
	D-28359 Bremen</address>
	</dd>
	<dt>Phone</dt>
	<dd>+49 (0)421 / 178 45-0</dd>
	<dt>Telefax</dt>
	<dd>+49 (0)421 / 178 45-4150</dd>
</dl>

<dl>
	<dt><abbr title="German Research Center for Artificial Intelligence">DFKI </abbr> Project Office Berlin</dt>
	<dd>
	<address>Alt-Moabit 91c<br>
	D-10559 Berlin</address>
	</dd>
	<dt>Phone</dt>
	<dd>+49 (0)30 / 238 95-0</dd>
</dl>

<dl>
	<dt>Management Board</dt>
	<dd>Prof. Dr. Antonio Krüger (CEO)</dd>
</dl>

<dl>
	<dt>Head of Supervisory Board</dt>
	<dd>Dr. Gabriël Clemens</dd>
</dl>

<dl>
	<dt>Register Court</dt>
	<dd>Amtsgericht Kaiserslautern</dd>
	<dt>Register Number</dt>
	<dd>HRB 2313</dd>
	<dt>ID-Nummer</dt>
	<dd>DE 148 646 97</dd>
	<dd>&nbsp;</dd>
</dl>
</section>

<section>
<p>Please read carefully the following Terms of Use (hereafter, "Terms") before using the Services (the "Services") proposed by the European Language Grid Consortium (the "Consortium") through https://www.european-language-grid.eu/ (the "Website").</p>

<p>By accessing or using the Services you agree to be bound by these Terms. If you disagree with any part of these Terms then you may not access the Services.</p>

<p><strong>1.&nbsp;&nbsp; &nbsp;Your Account</strong></p>

<p>Before using the Services, users (the "Users") agree that they should register by providing certain personal information as indicated in the ELG Privacy Policy in order to create an account (the "Account").</p>

<p>Please be aware that all information regarding the use of your personal information is contained in the ELG Privacy Policy. Therefore, before proceeding with registration to the Services we advise you to read this Policy carefully.</p>

<p>Users are responsible for maintaining the confidentiality of their Account, and of all activities occurring with this Account.&nbsp;</p>

<p>The Consortium waives all liability for misuse of the Account except in the case where such misuse causes a loss to the Consortium or has any effect on the security of the Services.&nbsp;</p>

<p>The Consortium reserves the right to delete an Account in the event a User misuse its Account in infringement of this Privacy Policy.</p>

<p><br>
<strong>2.&nbsp;&nbsp; &nbsp;Provision of Language Resources</strong></p>

<p>The Services allow Users to upload datasets, corpora, language models, source code and language technology tools and services, (collectively referred as the "Language Resources &amp; Technologies") on the platform for use by the language technology community.&nbsp;</p>

<p>By providing Language Resources &amp; Technologies on the Services, Users warrant that they have all legal rights and title on the Language Resources &amp; Technologies uploaded on the Platform or that they have obtained legal permission to perform this operation.&nbsp;</p>

<p>Users also warrant that the upload of the Language Resources &amp; Technologies will not infringe upon any third party’s rights of any kind, whether they would be Intellectual Property rights or any property right of any sort.&nbsp;</p>

<p>Users also warrant that the Language Resources &amp; Technologies do not violate any law or regulation.</p>

<p>When uploading a Language Resource or Technology on the Services, Users shall provide any information related to the Language Resources &amp; Technologies as required by the Services and the conditions under which the Language Resources &amp; Technologies shall be reused (the "License"). Users warrant that such information does not contain any misrepresentation of legal or material facts.&nbsp;<br>
&nbsp;<br>
The Consortium waives all liability for any infringement or violation caused by the upload of Language Resources &amp; Technologies on the Services.&nbsp;</p>

<p><strong>3.&nbsp;&nbsp; &nbsp;Usage of Language Resources</strong></p>

<p>Users that agree to download or run/execute Language Resources &amp; Technologies available on the Services acknowledge that they shall be reusing the Language Resources &amp; Technologies in compliance with the License set by the provider of the Language Resources &amp; Technologies.&nbsp;</p>

<p>The Consortium waives all liability for any infringement or violation caused by the reuse of Language Resources &amp; Technologies by the Users.&nbsp;</p>

<p><strong>4.&nbsp;&nbsp; &nbsp;Updates to Terms of Use</strong></p>

<p>The Consortium reserves the discretionary right to change the Terms of Use from time to time (referred as "Revised Terms of Use").</p>

<p>The Users shall be notified within a reasonable delay of any Revised Terms of Use by any means.&nbsp;</p>

<p>By using the Website or the Services after the effective date of the Revised Terms of Use, Users shall agree to the Revised Terms of Use.</p>

<p>The effective date of the Revised Terms of Use shall be the date indicated in the Revised Terms of Use.</p>

<p>&nbsp;</p>

<h2>Data Protection Notice</h2>

<p>The German Research Center for Artificial Intelligence (Deutsches Forschungszentrum für Künstliche Intelligenz — <abbr title="German Research Center for Artificial Intelligence">DFKI </abbr>) and its staff are committed to goal- and risk-oriented information privacy and the fundamental right to the protection of personal data. In this data protection policy we inform you about the processing of your personal data when visiting and using our web site.</p>

<h3>Controller</h3>

<dl>
	<dt>Phone:</dt>
	<dd>+49 (0)30/23895-1833</dd>
	<dt>E-Mail</dt>
	<dd><a href="http://mailto:georg.rehm@dfki.de" target="_blank">georg.rehm@dfki.de</a></dd>
</dl>

<h3>Data protection officer</h3>

<dl>
	<dt>Phone</dt>
	<dd>+49 (0)631 / 205 75-0</dd>
	<dt>E-Mail</dt>
	<dd><a href="http://mailto:datenschutz@dfki.de" target="_blank">datenschutz@dfki.de</a></dd>
</dl>

<h3>Purpose of data processing</h3>

<p>Provision of the information offering in the course of the public communication of the <abbr title="German Research Center for Artificial Intelligence">DFKI </abbr> Establishment of contact and correspondence with visitors and users.</p>

<h3>Anonymous and protected usage</h3>

<p>Visit and usage of our web site are anonymous. At our web site personal data are only collected to the technically necessary extent. The processed data will not be transmitted to any third parties or otherwise disclosed, except on the basis of concrete lawful obligations. Within our information offering we do not embed information or service offerings of third party providers.</p>

<p>While using our web site the data transmission in the internet is being protected by a generally accepted secure encryption procedure and hence cannot easily be eavesdropped or tampered.</p>

<h3>Access data</h3>

<p>On every access to our web site some usage, transmission and connection data will be collected, temporarily stored in a log file and regularly deleted after 90 days.</p>

<p>On every access/retrieval the following data are stored:</p>

<ul>
	<li><abbr title="Internet Protocol">IP </abbr> address</li>
	<li>transmitted user agent information (in particular type/version of web browser, operating system etc.)</li>
	<li>transmitted referrer information ( <abbr title="Uniform Resource Locator">URL </abbr> of the referring page)</li>
	<li>date and time of the access/retrieval</li>
	<li>transmitted access method/function</li>
	<li>transmitted input values (search terms etc.)</li>
	<li>retrieved page resp. file</li>
	<li>transmitted amount of data</li>
	<li>status of processing the access/retrieval</li>
</ul>

<p>The processing of the access data is lawful because it is necessary for the purposes of the legitimate interests pursued by <abbr title="German Research Center for Artificial Intelligence">DFKI </abbr>. The legitimate interests pursued by <abbr title="German Research Center for Artificial Intelligence">DFKI </abbr> are the adaptation and optimisation of the information offering and the investigation, detection and prosecution of illegal activities in connection with the usage of our web site.</p>

<p>The stored data records can be statistically evaluated in order to adapt and optimize our web site to the needs of our visitors. Any techniques that offer the possibility to retrace the access characteristics of users (tracking) will not be applied. The creation of user profiles and automated decision-making based on it is precluded.</p>

<p>The stored data records are not attributable to specific persons. They are generally not being combined with other data sources. However, the stored data can be analysed and combined with other data sources, if we become aware of concrete indications of any illegal usage.</p>

<h3>What personal data we collect and why we collect it</h3>

<h4>Contact forms</h4>

<p>Personal information sent through the contact form, in particular e-mail addresses, will be saved for only the purpose of responding to the message.</p>

<h4>Cookies</h4>

<p>We use so-called cookies on our web site. Cookies are small files that are being stored by your web browser. The cookies used on our web site do not harm your computer and do not contain any malicious software. They offer a user-friendly and effective usage of our web site. We do not use cookies for marketing purposes.</p>

<p>We transmit so-called session cookies to your web browser. They are valid only for the duration of your visit on our web site and they do not have any meaning outside of our web site. The session cookies are needed in order to identify your session with a unique number during your visit and to transmit our contents in your preferred language. At the end of your visit the session cookies will be automatically deleted upon termination of your web browser.</p>

<p>We also transmit permanent cookies to your web browser with a validity period of at most 365 days. We are exclusively using these cookies in order to respect your settings for the type of presentation (normal, inverted) and for the font size. Furthermore, it will be recorded whether you’ve taken notice of the information about the usage of cookies in your web browser.</p>

<p>You can adjust your web browser such that you will be informed on setting cookies and allow cookies on an individual basis resp. exclude the acceptance of cookies for specific cases or generally. You also can adjust the automatic deletion of cookies upon termination of your web browser. Upon deactivation of cookies the functionality of our web site can be limited. In any case, our information offering is available to its full extent.</p>

<h3>Embedded content from other websites</h3>

<p>Articles on this site may include embedded content (e.g. videos, images, articles, etc.). Embedded content from other websites behaves in the exact same way as if the visitor has visited the other website.</p>

<p>These websites may collect data about you, use cookies, embed additional third-party tracking, and monitor your interaction with that embedded content, including tracking your interaction with the embedded content if you have an account and are logged in to that website.</p>

<h4>Analytics</h4>

<p>This website uses Google Analytics, a web analytics service provided by Google, Inc. (“Google”). You can click in this link in order to access the privacy policy of Google Analytics. Google Analytics uses “cookies”, which are text files placed on your computer, to help the website analyze how users use the site. The information generated by the cookie about your use of the website will be transmitted to and stored by Google on servers in the United States.</p>

<p>In case <abbr title="Internet Protocol">IP </abbr>-anonymisation is activated on this website, your <abbr title="Internet Protocol">IP </abbr> address will be truncated within the area of Member States of the European Union or other parties to the Agreement on the European Economic Area. Only in exceptional cases the whole <abbr title="Internet Protocol">IP </abbr> address will be first transfered to a Google server in the USA and truncated there. The <abbr title="Internet Protocol">IP </abbr>-anonymisation is active on this website.</p>

<p>Google will use this information on behalf of the operator of this website for the purpose of evaluating your use of the website, compiling reports on website activity for website operators and providing them other services relating to website activity and internet usage.</p>

<p>The <abbr title="Internet Protocol">IP </abbr>-address, that your Browser conveys within the scope of Google Analytics, will not be associated with any other data held by Google. You may refuse the use of cookies by selecting the appropriate settings on your browser, however please note that if you do this you may not be able to use the full functionality of this website. You can also opt-out from being tracked by Google Analytics with effect for the future by downloading and installing Google Analytics Opt-out Browser Addon for your current web browser: http://tools.google.com/dlpage/gaoptout?hl=en.</p>

<p>As an alternative to the browser Addon or within browsers on mobile devices, you can click this link <a data-ua="UA-136240201-1" href="https://www.european-language-grid.eu/privacy-policy/#" target="_blank">Disable Google Analytics</a> in order to opt-out from being tracked by Google Analytics within this website in the future (the opt-out applies only for the browser in which you set it and within this domain). An opt-out cookie will be stored on your device, which means that you’ll have to click this link again, if you delete your cookies.</p>

<h3>Correspondence</h3>

<p>You have the option to contact us by e-mail. We will use your e-mail address and other personal contact data for the correspondence with you. Due to lawful obligation every e-mail correspondence will be archived. Subject to our legitimate interests your e-mail address and other personal contact data can be stored in our contact data base. In this case you will receive a corresponding information on the processing of your contact data.</p>

<h3>Access and Intervention</h3>

<p>Besides the information in this data protection policy you have the right of access to your personal data. To ensure fair data processing, you have the following rights:</p>

<ul>
	<li>The right to rectification and completion of your personal data</li>
	<li>The right to erasure of your personal data</li>
	<li>The right to restriction of the processing of your personal data</li>
	<li>The right to object to the processing of your personal data on grounds related to your particular situation</li>
	<li>To exercise these rights, please contact our data protection officer.</li>
</ul>

<h3>Right to lodge a complaint</h3>

<p>You have the right to lodge a complaint with a supervisory authority if you consider that the processing of your personal data infringes statutory data protection regulations.</p>
</section>
</section>
    
    </div>
    <form class="form-actions" action="${url.loginAction}" method="POST">
        <input class="${properties.kcButtonClass!} ${properties.kcButtonPrimaryClass!} ${properties.kcButtonLargeClass!}" name="accept" id="kc-accept" type="submit" value="${msg("doAccept")}"/>
        <input class="${properties.kcButtonClass!} ${properties.kcButtonDefaultClass!} ${properties.kcButtonLargeClass!}" name="cancel" id="kc-decline" type="submit" value="${msg("doDecline")}"/>
    </form>
    <div class="clearfix"></div>
    </#if>
</@layout.registrationLayout>
